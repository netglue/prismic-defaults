<?php

return [

    'prismic' => [
        /**
         * Define the bookmark name used to retrieve the site-wide configuration object
         */
        'config-bookmark' => 'site-config',
        /**
         * Define the bookmarks used to retrieve content
         * for 404 and 500 error responses
         */
        'error-404-bookmark' => 'error-404',
        'error-500-bookmark' => 'error-500',

        /**
         * Define which document types are searchable
         */
         'search' => [
            'documentTypes' => [
                'page'
            ],
         ],

         /**
          * Map Slices by name to specific view scripts
          */
        'slice-map' => [
            'dynamic-content' => 'netglue-prismic/dynamic-content',
            'carousel'        => 'netglue-prismic/carousel',
            'featured-items'  => 'netglue-prismic/featured-items',
            'banner'          => 'netglue-prismic/banner',
            'separator'       => 'netglue-prismic/separator',
            'content'         => 'netglue-prismic/content',
            'block-heading'   => 'netglue-prismic/block-heading',
            'faq-section'     => 'netglue-prismic/faq',
            'call-to-action'  => 'netglue-prismic/call-to-action',
            'contact-form'    => 'netglue-prismic/contact-form',
            'search-form'     => 'netglue-prismic/search-form',
            'subscribe-form'  => 'netglue-prismic/content-subscribe-form',
            'code-block'      => 'netglue-prismic/code-block',
            'featured-links'  => 'netglue-prismic/featured-links',
            'content-block'   => 'netglue-prismic/content-block',
            'quote'           => 'netglue-prismic/quote',
        ],

    ],

    'router' => [
        'routes' => [
            'netglue/clear-content-cache' => [
                'type'    => 'Literal',
                'options' => [
                    'route'    => '/ng/clear-content-cache',
                    'defaults' => [
                        'controller' => 'NetgluePrismicDefaults\Mvc\Controller\ContentController',
                        'action'     => 'cache-purge',
                    ],
                ],
            ],
            'netglue-search' => [
                'type' => 'Literal',
                'options' => [
                    'route' => '/search',
                    'defaults' => [
                        'controller' => 'NetgluePrismicDefaults\Mvc\Controller\SearchController',
                        'action'     => 'search',
                        'bookmark'   => 'search',
                        'view'       => 'netglue-prismic/search',
                    ],
                ],
                'may_terminate' => true,
                'child_routes'  => [
                    'paged' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'    => '[/page/:page][/q/:q]',
                            'defaults' => [ 'page' => 1, ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'view_manager' => [

        'template_map' => [

            /**
             * Error Pages
             */
            'error/500' => __DIR__ . '/../view/error/internal-error.phtml',
            'error/404' => __DIR__ . '/../view/error/not-found.phtml',

            /**
             * Content Pages
             */
            'netglue-prismic/page'              => __DIR__ . '/../view/pages/page.phtml',
            'netglue-prismic/search'            => __DIR__ . '/../view/pages/search.phtml',

            /**
             * Partials
             */
            'netglue-prismic/search-result'           => __DIR__ . '/../view/partial/search-result.phtml',
            'netglue-prismic/search-pager'            => __DIR__ . '/../view/partial/search-pager.phtml',
            'netglue-prismic/bootstrap-nav'           => __DIR__ . '/../view/partial/bootstrap-nav.phtml',
            'netglue-prismic/quote-markup'            => __DIR__ . '/../view/partial/quote-markup.phtml',
            'netglue-prismic/single-quote'            => __DIR__ . '/../view/partial/single-quote.phtml',
            'netglue-prismic/multiple-quotes'         => __DIR__ . '/../view/partial/multiple-quotes.phtml',

            /**
             * Slices
             */
            'netglue-prismic/dynamic-content'         => __DIR__ . '/../view/slices/dynamic-content.phtml',
            'netglue-prismic/carousel'                => __DIR__ . '/../view/slices/carousel.phtml',
            'netglue-prismic/banner'                  => __DIR__ . '/../view/slices/banner.phtml',
            'netglue-prismic/featured-items'          => __DIR__ . '/../view/slices/featured-items.phtml',
            'netglue-prismic/separator'               => __DIR__ . '/../view/slices/separator.phtml',
            'netglue-prismic/content'                 => __DIR__ . '/../view/slices/content.phtml',
            'netglue-prismic/block-heading'           => __DIR__ . '/../view/slices/block-heading.phtml',
            'netglue-prismic/faq'                     => __DIR__ . '/../view/slices/faq.phtml',
            'netglue-prismic/call-to-action'          => __DIR__ . '/../view/slices/call-to-action.phtml',
            'netglue-prismic/contact-form'            => __DIR__ . '/../view/slices/contact-form.phtml',
            'netglue-prismic/search-form'             => __DIR__ . '/../view/slices/search-form.phtml',
            'netglue-prismic/code-block'              => __DIR__ . '/../view/slices/code-block.phtml',
            'netglue-prismic/featured-links'          => __DIR__ . '/../view/slices/featured-links.phtml',
            'netglue-prismic/content-subscribe-form'  => __DIR__ . '/../view/slices/content-subscribe-form.phtml',
            'netglue-prismic/content-block'           => __DIR__ . '/../view/slices/content-block.phtml',
            'netglue-prismic/quote'                   => __DIR__ . '/../view/slices/quote.phtml',

        ],
    ],

];
