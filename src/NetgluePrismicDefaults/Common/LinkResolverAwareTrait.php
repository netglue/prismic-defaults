<?php

namespace NetgluePrismicDefaults\Common;

use Prismic\LinkResolver;

trait LinkResolverAwareTrait
{
    /**
     * @var LinkResolver
     */
    protected $linkResolver;

    /**
     * Set Link Resolver
     * @param  LinkResolver $resolver
     * @return self
     */
    public function setLinkResolver(LinkResolver $resolver)
    {
        $this->linkResolver = $resolver;

        return $this;
    }

    /**
     * Get Link Resolver
     * @return LinkResolver|null
     */
    public function getLinkResolver()
    {
        return $this->linkResolver;
    }
}
