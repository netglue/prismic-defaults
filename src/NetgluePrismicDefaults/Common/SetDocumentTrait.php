<?php

namespace NetgluePrismicDefaults\Common;

use Prismic\Document;

trait SetDocumentTrait
{

    /**
     * Document
     * @var Document
     */
    private $document;

    /**
     * Set Document
     * @param Document $document
     * @return self
     */
    public function setDocument(Document $document)
    {
        $this->document = $document;

        return $this;
    }

    /**
     * Get document
     * @return Document|null
     */
    public function getDocument()
    {
        return $this->document;
    }

}
