<?php

namespace NetgluePrismicDefaults\Service;

use NetgluePrismic\ContextAwareInterface;
use NetgluePrismic\ContextAwareTrait;
use NetgluePrismic\ApiAwareInterface;
use NetgluePrismic\ApiAwareTrait;
use NetgluePrismicDefaults\Exception;
use Prismic\Response;
use Zend\Cache\StorageInterface as Cache;

abstract class AbstractDocumentService
{

    use ContextAwareTrait;
    use ApiAwareTrait;

    /**
     * The document type we're looking for
     * @var string
     */
    protected $type = null;

    /**
     * Cache
     * @var Cache
     */
    private $cache;

    /**
     * Fragment to order on by default
     * @var string|null
     */
    protected $defaultOrder;

    /**
     * Fragment to order on
     * @var string|null
     */
    protected $order;

    /**
     * Direction of default ordering
     * @var string
     */
    protected $defaultOrderDirection = 'asc';

    /**
     * Set the document type that we're looking for
     * @param string $type
     * @return void
     */
    public function setDocumentType($type)
    {
        if(empty($type) || !is_string($type)) {
            throw new \InvalidArgumentException(sprintf(
                'Document type must be a non-empty string representing a Prismic document mask, recieved %s',
                gettype($type)
            ));
        }
        $this->type = (string) $type;
    }

    /**
     * Return document type
     * @return string
     */
    public function getDocumentType()
    {
        return $this->type;
    }

    /**
     * Provide a cache instance for caching results
     * @param Cache $cache
     * @return void
     */
    public function setCache(Cache $cache = null)
    {
        $this->cache = $cache;
    }

    /**
     * Return configured cache instance
     * @return Cache|null
     */
    public function getCache()
    {
        return $this->cache;
    }

    /**
     * Whether a cache instance is available
     * @return bool
     */
    public function hasCache()
    {
        return $this->cache instanceof Cache;
    }

    /**
     * Set the default fragment used to order results
     * @param string $fragmentName
     * @return void
     */
    public function setDefaultOrder($fragmentName)
    {
        $this->defaultOrder = $fragmentName;
    }

    /**
     * Set the default order direction
     * @param bool $asc
     * @return void
     */
    public function setDefaultOrderDirection($asc = true) {
        $this->defaultOrderDirection = ((bool) $asc) ? 'asc' : 'desc';
    }

    /**
     * Return fragment name used for ordering result
     * @return string|null
     */
    public function getDefaultOrderFragment()
    {
        if($this->defaultOrder) {
            return $this->normaliseFragmentName($this->defaultOrder);
        }
        return null;
    }

    /**
     * Return the default order direction
     *
     * Note: Ascending is the default in the Prismic API and setting order to 'asc' will cause errors
     * Therefore, if the default ordering has been set to asc, this method will return an empty string
     *
     * @return string
     */
    public function getDefaultOrderDirection()
    {
        return $this->defaultOrderDirection === 'asc' ? '' : strtolower($this->defaultOrderDirection);
    }

    /**
     * Helper to normalise a fragment name
     * @param string $name
     * @throws \Exception
     * @return string
     */
    protected function normaliseFragmentName($name) {
        if(!is_string($name) || empty($name)) {
            throw new Exception\InvalidArgumentException('Invalid or empty fragment name');
        }
        if(strpos($name, $this->type) === 0) {
            return $name;
        }
        if(strpos($name, '.') !== false) {
            throw new Exception\RuntimeException(sprintf(
                'Found a dot in the fragment name [%s] but does not match configured mask/type of %s',
                $name,
                $this->type
            ));
        }
        return $this->type . '.' . $name;
    }

    /**
     * Perform a query and return the response
     * @param array $predicates
     * @param string $orderings
     * @param int $pageSize
     * @return Response
     */
    public function query(array $predicates, $orderings = null, $pageSize = null)
    {
        $api = $this->getPrismicApi();
        $ref = $this->getContext()->getRefAsString();
        $form = $api->forms()->everything
                ->ref($ref)
                ->query($predicates);
        if(!empty($pageSize)) {
            $form = $form->pageSize($pageSize);
        }
        if(!empty($orderings)) {
            $form = $form->orderings($orderings);
        }

        return $form->submit();
    }

}
