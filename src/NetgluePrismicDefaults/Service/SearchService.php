<?php

namespace NetgluePrismicDefaults\Service;

use NetgluePrismic\ContextAwareInterface;
use NetgluePrismic\ContextAwareTrait;
use NetgluePrismic\ApiAwareInterface;
use NetgluePrismic\ApiAwareTrait;
use Prismic\Document;
use Prismic\SearchForm;
use Prismic\Predicates;
use Prismic\Response;

use Zend\Paginator\Paginator;
use NetgluePrismic\Paginator\Adapter\SearchFormAdapter;

class SearchService implements ContextAwareInterface,
                               ApiAwareInterface
{

    use ContextAwareTrait,
        ApiAwareTrait;

    /**
     * Types of Document to constrain Search to
     * @var array
     */
    private $types = [];

    /**
     * Set the document type search constraint
     * @param array $types
     */
    public function setDocumentTypes(array $types)
    {
        $this->types = $types;
    }


    /**
     * Return a paginator that will page though all returned documents
     * @param string $query
     * @param int $perPage
     * @return Paginator
     */
    public function search($query, $perPage = null)
    {
        $api        = $this->getPrismicApi();
        $ref        = $this->getContext()->getRefAsString();
        $escaper = new \Zend\Escaper\Escaper;
        //$query = $escaper->escapeUrl($query);
        $query = str_replace('"','\"', $query);
        $predicates = array(
            Predicates::any("document.type", $this->types),
            Predicates::fulltext("document", $query),
        );
        $form = $api->forms()->everything
                ->ref($ref)
                ->query($predicates);

        $pager = $this->createPager($form);
        $pager->setItemCountPerPage($perPage);

        return $pager;
    }


    /**
     * Create a pager for the given Prismic Search Form
     * @param SearchForm $form
     * @return Paginator
     */
    public function createPager(SearchForm $searchForm)
    {
        $adapter = new SearchFormAdapter($searchForm);
        return new Paginator($adapter);
    }

    /**
     * Perform a query and return the response
     * @param array $predicates
     * @param string $orderings
     * @param int $pageSize
     * @return Response
     */
    public function query(array $predicates, $orderings = null, $pageSize = null)
    {
        $api = $this->getPrismicApi();
        $ref = $this->getContext()->getRefAsString();
        $form = $api->forms()->everything
                ->ref($ref)
                ->query($predicates);
        if(!empty($pageSize)) {
            $form = $form->pageSize($pageSize);
        }
        if(!empty($orderings)) {
            $form = $form->orderings($orderings);
        }

        return $form->submit();
    }

}
