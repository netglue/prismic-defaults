<?php
namespace NetgluePrismicDefaults\Exception;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
