<?php
namespace NetgluePrismicDefaults\Exception;

class RuntimeException extends \RuntimeException implements ExceptionInterface
{
}
