<?php

namespace NetgluePrismicDefaults\View\Helper;

use NetgluePrismicDefaults\Service\SiteConfig as Config;
use Zend\View\Helper\AbstractHelper;

class SiteConfigHelper extends AbstractHelper
{
    /**
     * Site Wide Config Object
     * @var Config
     */
    private $config;

    /**
     * @param  Config $config
     * @return void
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Proxy to site-wide config
     */
    public function __call($method, $args)
    {
        return call_user_func_array(array($this->config, $method), $args);
    }

}
