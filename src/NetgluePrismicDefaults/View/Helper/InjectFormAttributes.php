<?php

namespace NetgluePrismicDefaults\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Prismic\Document;
use Zend\Form\Form;
use NetgluePrismicDefaults\Model\FormConfig;


class InjectFormAttributes extends AbstractHelper
{

    private $form;
    private $config;

    public function __invoke(Form $form, Document $config)
    {
        $this->form = $form;
        $this->config = new FormConfig($config);
        $this->prepareForm();
    }

    private function prepareForm()
    {
        foreach($this->form->getElements() as $element) {
            $this->setElementLabel($element);
            $this->setElementPlaceholder($element);
        }
    }

    private function setElementLabel($element)
    {
        $name = $element->getName();
        if($label = $this->config->getLabel($name)) {
            $element->setLabel($label);
        }
    }

    private function setElementPlaceholder($element)
    {
        $name = $element->getName();
        if($placeholder = $this->config->getPlaceholder($name)) {
            $element->setAttribute('placeholder', $placeholder);
        }
    }

}
