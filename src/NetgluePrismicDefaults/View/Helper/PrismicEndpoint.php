<?php

/**
 * A View helper that returns the configured API endpoint
 */

namespace NetgluePrismicDefaults\View\Helper;

use Zend\View\Helper\AbstractHelper;

class PrismicEndpoint extends AbstractHelper
{

    /**
     * @var string
     */
    private $endpoint;

    public function __construct($endpoint)
    {
        $this->endpoint = $endpoint;
    }

    public function __invoke()
    {
        return $this->endpoint;
    }

}
