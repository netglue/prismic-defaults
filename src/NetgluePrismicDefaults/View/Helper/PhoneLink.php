<?php

namespace NetgluePrismicDefaults\View\Helper;

use NetgluePrismicDefaults\Service\SiteConfig as Config;
use Zend\View\Helper\AbstractHelper;

class PhoneLink extends AbstractHelper
{

    private $config;

    private $format = '<a href="tel:%1$s">%2$s</a>';

    public function __construct(Config $config)
    {
        $this->config = $config;
    }


    public function __invoke($number = null, $label = null, $format = null)
    {
        $number = $number ? $number : $this->config->get('phone');
        $label  = $label  ? $label  : $number;
        $number = urlencode(str_replace(' ', '', $number));
        $format = $format ? $format : $this->format;

        return sprintf($format, $number, $label);
    }

}
