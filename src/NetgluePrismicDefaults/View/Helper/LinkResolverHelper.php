<?php

namespace NetgluePrismicDefaults\View\Helper;

use NetgluePrismic\Mvc\LinkResolver;
use Prismic\Document;
use Prismic\Fragment\Link\LinkInterface;
use Zend\View\Helper\AbstractHelper;

class LinkResolverHelper extends AbstractHelper
{
    /**
     * @var LinkResolver
     */
    private $resolver;

    /**
     * @param  LinkResolver $resolver
     * @return void
     */
    public function __construct(LinkResolver $resolver)
    {
        $this->resolver = $resolver;
    }

    public function __invoke()
    {
        return $this;
    }

    public function get()
    {
        return $this->resolver;
    }

    public function resolve($target)
    {
        if ($target instanceof LinkInterface) {
            return $target->getUrl($this->resolver);
        }

        if ($target instanceof Document) {
            return $this->getView()->prismicUrl($target);
        }

        return '#';
    }
}
