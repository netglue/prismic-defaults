<?php

namespace NetgluePrismicDefaults\View\Helper;

use Zend\View\Helper\AbstractHelper;
use NetgluePrismicDefaults\Common\SetDocumentTrait;
use Prismic\Document;
use Prismic\Fragment;

class ContentSlices extends AbstractHelper
{

    use SetDocumentTrait;

    /**
     * Maps content slice names to view scripts
     * @var array
     */
    private $mappings = [];


    /**
     * Construct with view script hash
     * @param array $mappings
     * @return void
     */
    public function __construct(array $mappings)
    {
        $this->mappings = $mappings;
    }

    /**
     * Invoke
     *
     * Renders the slice zone for the document in $fragmentName
     *
     * @param  Document $document
     * @param  string $fragmentName
     *
     * @return string
     */
    public function __invoke(Document $document, $fragmentName = 'body')
    {
        $out = '';
        if ($zone = $this->getSliceZone($document, $fragmentName)) {
            foreach($zone->getSlices() as $slice) {
                $out .= $this->sliceAsString($document, $slice);
            }
        }

        return $out;
    }

    /**
     * Return the slice zone from the document
     *
     * The fragment must exist in the document and it must also be a Fragment\SliceZone
     * otherwise null is returned
     *
     * @param  Document $document
     * @param  string $fragmentName
     *
     * @return Fragment\SliceZone|null
     */
    private function getSliceZone(Document $document, $fragmentName)
    {
        $type = $document->getType();
        $fragName = sprintf('%s.%s', $type, $fragmentName);
        if ($zone = $document->get($fragName)) {
            if ($zone instanceof Fragment\SliceZone) {
                return $zone;
            }
        }
    }

    /**
     * Render a single slice
     *
     * @param  Document $document
     * @param  Fragment\Slice $slice
     *
     * @return string
     */
    private function sliceAsString(Document $document, Fragment\SliceInterface $slice)
    {
        $type = $slice->getSliceType();
        if (isset($this->mappings[$type])) {
            $partial = $this->mappings[$type];
            $model = [
                'document' => $document,
                'slice'    => $slice,
            ];

            return (string) $this->view->partial($partial, $model);
        }

        return '';
    }

}
