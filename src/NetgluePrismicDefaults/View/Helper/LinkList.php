<?php

namespace NetgluePrismicDefaults\View\Helper;

use NetgluePrismic\Context;
use NetgluePrismic\Mvc\LinkResolver;
use NetgluePrismicDefaults\Exception;
use Prismic\Document;
use Prismic\Fragment\FragmentInterface;
use Prismic\Fragment\Link\DocumentLink;
use Prismic\Fragment\Link\LinkInterface;
use Zend\View\Helper\AbstractHelper;

class LinkList extends AbstractHelper
{
    private $linkResolver;

    private $context;

    private $partial;

    private $document;

    public function __construct(Context $context, LinkResolver $resolver)
    {
        $this->context = $context;
        $this->linkResolver = $resolver;
    }

    public function __invoke(Document $list = null)
    {
        if($list) {
            $this->setLinkList($list);
        }
        return $this;
    }

    public function setPartial($partial)
    {
        $this->partial = $partial;

        return $this;
    }

    public function getPartial()
    {
        return $this->partial;
    }
    
    public function __toString()
    {
        return $this->render();
    }
    
    public function render()
    {
        if(!$this->document) {
            return '';
        }
        if($this->partial) {
            return (string) $this->getView()->partial($this->partial, ['list' => $this->getLinkList()]);
        }
        return $this->parseLinkList();
    }

    public function setLinkList(Document $list)
    {
        if ($list->getType() !== 'link-list') {
            throw new Exception\InvalidArgumentException('Expected a document of type \'link-list\'');
        }

        $this->document = $list;

        return $this;
    }

    public function getLinkList()
    {
        return $this->document;
    }

    public function parseLinkList(Document $list = null)
    {
        $list = $list ? $list : $this->document;
        if(!$list) {
            return '';
        }
        $links = $list->get('link-list.links')->getArray();
        $markup = [];
        foreach($links as $link) {
            if(!isset($link['url'])) {
                continue;
            }
            $class = isset($link['class']) ? $link['class'] : null;
            $title = isset($link['title']) ? $link['title'] : null;
            $text = isset($link['text']) ? $link['text'] : null;
            $markup[] = $this->parseLink($link['url'], $text, $class, $title);
        }

        return sprintf(
            '<ul>%s</ul>',
            implode("\n", $markup)
        );
    }


    public function parseLink(LinkInterface $link, FragmentInterface $text, FragmentInterface $class = null, FragmentInterface $title = null)
    {
        $url        = $this->linkResolver->resolve($link);
        $className  = $class ? $this->view->escapeHtmlAttr($class->asText()) : '';
        $titleText  = $title ? $this->view->escapeHtmlAttr($title->asText()) : '';
        $anchorText = $text  ? $this->view->escapeHtml($text->asText()) : '';
        $subMenu    = '';

        if($link instanceof DocumentLink) {
            $target = $this->context->getDocumentById($link->getId());
            if($target && $target->getType() === 'link-list') {
                $url = '#';
                $subMenu = $this->parseLinkList($target);
            }
        }

        if(!$url) {
            return null;
        }

        $linkFormat = '<li><a href="%s" class="%s" title="%s">%s</a>%s</li>';
        return sprintf(
            $linkFormat,
            $url,
            $className,
            $titleText,
            $anchorText,
            $subMenu
        );
    }
}
