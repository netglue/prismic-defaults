<?php

namespace NetgluePrismicDefaults\View\Helper;

use NetgluePrismicDefaults\Model\FormConfig;
use Zend\View\Helper\AbstractHelper;
use Prismic\Document;

class FormHelpBlock extends AbstractHelper
{

    public function __invoke($name, Document $config)
    {
        $config = new FormConfig($config);
        if($resolver = $this->view->linkResolver()->get()) {
            $config->setLinkResolver($resolver);
        }
        return $config->getHelpBlock($name);
    }

}
