<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\View\Helper\LinkResolverHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LinkResolverHelperFactory implements FactoryInterface
{
    /**
     * Return Link Resolver Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return LinkResolverHelper
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $resolver = $serviceLocator->get('NetgluePrismic\Mvc\LinkResolver');
        return new LinkResolverHelper($resolver);
    }
}
