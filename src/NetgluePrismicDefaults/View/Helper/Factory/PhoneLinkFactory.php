<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\View\Helper\PhoneLink;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PhoneLinkFactory implements FactoryInterface
{
    /**
     * Return PhoneLink Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return LinkList
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $config = $serviceLocator->get('NetgluePrismicDefaults\Service\SiteConfig');

        return new PhoneLink($config);
    }
}
