<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\View\Helper\ContentSlices;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ContentSlicesFactory implements FactoryInterface
{
    /**
     * Return ContentSlices Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return ContentSlices
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $config = $serviceLocator->get('Config');
        $mappings = isset($config['prismic']['slice-map']) ? $config['prismic']['slice-map'] : [];


        return new ContentSlices($mappings);
    }
}
