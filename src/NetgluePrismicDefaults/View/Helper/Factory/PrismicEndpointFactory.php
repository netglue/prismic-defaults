<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\Exception;
use NetgluePrismicDefaults\View\Helper\PrismicEndpoint;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class PrismicEndpointFactory implements FactoryInterface
{
    /**
     * Return PrismicEndpoint Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return PrismicEndpoint
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $config = $serviceLocator->get('Config');
        if (!isset($config['prismic']['api'])) {
            throw new Exception\RuntimeException('No API endpoint has been configured');
        }
        return new PrismicEndpoint($config['prismic']['api']);
    }

}
