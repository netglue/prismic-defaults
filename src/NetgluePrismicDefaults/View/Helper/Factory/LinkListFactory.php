<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\View\Helper\LinkList;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class LinkListFactory implements FactoryInterface
{
    /**
     * Return Link List Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return LinkList
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $context = $serviceLocator->get('NetgluePrismic\Context');
        $resolver = $serviceLocator->get('NetgluePrismic\Mvc\LinkResolver');


        return new LinkList($context, $resolver);
    }
}
