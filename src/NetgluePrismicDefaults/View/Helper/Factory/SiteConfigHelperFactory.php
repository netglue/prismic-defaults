<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\View\Helper\SiteConfigHelper;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SiteConfigHelperFactory implements FactoryInterface
{
    /**
     * Return Site Config Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return SiteConfigHelper
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $config = $serviceLocator->get('NetgluePrismicDefaults\Service\SiteConfig');
        return new SiteConfigHelper($config);
    }
}
