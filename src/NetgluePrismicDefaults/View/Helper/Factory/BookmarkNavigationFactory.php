<?php

namespace NetgluePrismicDefaults\View\Helper\Factory;

use NetgluePrismicDefaults\View\Helper\BookmarkNavigation;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class BookmarkNavigationFactory implements FactoryInterface
{
    /**
     * Return Link List Helper
     * @param ServiceLocatorInterface $viewPluginManager
     * @return LinkList
     */
    public function createService(ServiceLocatorInterface $viewPluginManager)
    {
        $serviceLocator = $viewPluginManager->getServiceLocator();
        $context = $serviceLocator->get('NetgluePrismic\Context');

        return new BookmarkNavigation($context);
    }
}
