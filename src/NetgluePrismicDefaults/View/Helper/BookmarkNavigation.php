<?php

namespace NetgluePrismicDefaults\View\Helper;

use NetgluePrismic\Context;
use NetgluePrismicDefaults\Exception;
use Zend\View\Helper\AbstractHelper;

class BookmarkNavigation extends AbstractHelper
{
    
    private $context;
    
    public function __construct(Context $context)
    {
        $this->context = $context;
    }
    
    public function __invoke($bookmark, $partial)
    {
        $document = $this->context->getDocumentByBookmark($bookmark);
        if(!$document) {
            throw new Exception\InvalidArgumentException(sprintf(
                'The bookmark %s does not resolve to a document',
                $bookmark
            ));
        }
        return (string) $this->getView()->linkList($document)->setPartial($partial);
    }
    
}