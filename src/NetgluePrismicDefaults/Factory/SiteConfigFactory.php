<?php

namespace NetgluePrismicDefaults\Factory;

use NetgluePrismic\Exception\ExceptionInterface as PrismicException;
use NetgluePrismicDefaults\Service\SiteConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SiteConfigFactory implements FactoryInterface
{
    /**
     * Return SiteConfig
     * @param ServiceLocatorInterface $serviceLocator
     * @return SiteConfig
     * @throws \RuntimeException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config = $serviceLocator->get('Config');

        if (!isset($config['prismic']['config-bookmark'])) {
            throw new \RuntimeException('Cannot initialise Site-wide config unless a bookmark has been configured');
        }

        try {
            $context = $serviceLocator->get('Prismic\Context');
            $document = $context->getDocumentByBookmark($config['prismic']['config-bookmark']);
        } catch (PrismicException $e) {
            throw new \RuntimeException('Failed to load site-wide configuration bookmark from repository', null, $e);
        }

        $linkResolver = $serviceLocator->get('NetgluePrismic\Mvc\LinkResolver');

        $site = new SiteConfig($document, $linkResolver);
        return $site;
    }
}
