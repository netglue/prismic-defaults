<?php

namespace NetgluePrismicDefaults\Factory;

use NetgluePrismic\Exception\ExceptionInterface as PrismicException;
use NetgluePrismicDefaults\Service\SearchService;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SearchServiceFactory implements FactoryInterface
{
    /**
     * Return SearchService
     * @param ServiceLocatorInterface $serviceLocator
     * @return SearchService
     * @throws \RuntimeException
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $config  = $serviceLocator->get('Config');
        $context = $serviceLocator->get('Prismic\Context');
        $api     = $serviceLocator->get('Prismic\Api');
        $repo    = new SearchService;

        $repo->setContext($context);
        $repo->setPrismicApi($api);
        $repo->setDocumentTypes($config['prismic']['search']['documentTypes']);
        return $repo;
    }
}
