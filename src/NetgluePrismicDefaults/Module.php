<?php

namespace NetgluePrismicDefaults;

use Zend\EventManager\EventInterface;
use Zend\Loader\AutoloaderFactory;
use Zend\Loader\StandardAutoloader;
use Zend\ModuleManager\Feature\AutoloaderProviderInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ControllerProviderInterface;
use Zend\ModuleManager\Feature\DependencyIndicatorInterface;
use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\Mvc\MvcEvent;
use Zend\Http\Request as HttpRequest;
use Zend\Console\Request as ConsoleRequest;

class Module implements
    AutoloaderProviderInterface,
    ConfigProviderInterface,
    ControllerProviderInterface,
    ServiceProviderInterface,
    DependencyIndicatorInterface,
    ViewHelperProviderInterface,
    BootstrapListenerInterface
{
    /**
     * Expected to return an array of modules on which the current one depends on
     *
     * @return array
     */
    public function getModuleDependencies()
    {
        return [
            'NetgluePrismic',
        ];
    }

    /**
     * Return autoloader configuration
     * @link http://framework.zend.com/manual/2.0/en/user-guide/modules.html
     * @return array
     */
    public function getAutoloaderConfig()
    {
        return [
            AutoloaderFactory::STANDARD_AUTOLOADER => [
                StandardAutoloader::LOAD_NS => [
                    __NAMESPACE__ => __DIR__,
                ],
            ],
        ];
    }

    /**
     * Include/Return module configuration
     * @return array
     * @implements ConfigProviderInterface
     */
    public function getConfig()
    {
        return include __DIR__ . '/../../config/module.config.php';
    }

    /**
     * Return Controller Config
     * @return array
     */
    public function getControllerConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicDefaults\Mvc\Controller\ContentController'   => 'NetgluePrismicDefaults\Mvc\Factory\ContentControllerFactory',
                'NetgluePrismicDefaults\Mvc\Controller\ContactController'   => 'NetgluePrismicDefaults\Mvc\Factory\ContactControllerFactory',
                'NetgluePrismicDefaults\Mvc\Controller\SearchController'    => 'NetgluePrismicDefaults\Mvc\Factory\SearchControllerFactory',
                'NetgluePrismicDefaults\Mvc\Controller\SubscribeController' => 'NetgluePrismicDefaults\Mvc\Factory\SubscribeControllerFactory',
            ],
        ];
    }

    /**
     * Return controller plugin config
     * @return array
     * @implements ControllerPluginProviderInterface
     */
    public function getControllerPluginConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicDefaults\Mvc\Controller\Plugin\SiteConfig' => 'NetgluePrismicDefaults\Mvc\Controller\Plugin\Factory\SiteConfigFactory',
            ],
            'aliases' => [
                'siteConfig' => 'NetgluePrismicDefaults\Mvc\Controller\Plugin\SiteConfig',
            ],
        ];
    }

    /**
     * Return Service Config
     * @return array
     * @implements ServiceProviderInterface
     */
    public function getServiceConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicDefaults\Service\SiteConfig'                 => 'NetgluePrismicDefaults\Factory\SiteConfigFactory',
                'NetgluePrismicDefaults\Service\SearchService'              => 'NetgluePrismicDefaults\Factory\SearchServiceFactory',
                'NetgluePrismicDefaults\Service\Listener\MandrillListener'  => 'NetgluePrismicDefaults\Service\Listener\Factory\MandrillListenerFactory',
                'NetgluePrismicDefaults\Captcha\ReCaptchaAdapter'           => 'NetgluePrismicDefaults\Form\Factory\ReCaptchaAdapterFactory',
                'NetgluePrismicDefaults\Service\SubscriptionService'        => 'NetgluePrismicDefaults\Factory\SubscriptionServiceFactory',
                'NetgluePrismicDefaults\Service\Listener\SubscribeListener' => 'NetgluePrismicDefaults\Service\Listener\Factory\SubscribeListenerFactory',
                'NetgluePrismicDefaults\Form\SubscribeForm'                 => 'NetgluePrismicDefaults\Form\Factory\SubscribeFormFactory',
                'NetgluePrismicDefaults\Mvc\Listener\HeadMetaListener'      => 'NetgluePrismicDefaults\Mvc\Factory\HeadMetaListenerFactory',
            ],

            'aliases' => [],
        ];
    }

    /**
     * Return view helper plugin config
     * @return array
     * @implements ViewHelperProviderInterface
     */
    public function getViewHelperConfig()
    {
        return [
            'factories' => [
                'NetgluePrismicDefaults\View\Helper\ContentSlices'        => 'NetgluePrismicDefaults\View\Helper\Factory\ContentSlicesFactory',
                'NetgluePrismicDefaults\View\Helper\LinkResolverHelper'   => 'NetgluePrismicDefaults\View\Helper\Factory\LinkResolverHelperFactory',
                'NetgluePrismicDefaults\View\Helper\SiteConfigHelper'     => 'NetgluePrismicDefaults\View\Helper\Factory\SiteConfigHelperFactory',
                'NetgluePrismicDefaults\View\Helper\LinkList'             => 'NetgluePrismicDefaults\View\Helper\Factory\LinkListFactory',
                'NetgluePrismicDefaults\View\Helper\BookmarkNavigation'   => 'NetgluePrismicDefaults\View\Helper\Factory\BookmarkNavigationFactory',
                'NetgluePrismicDefaults\View\Helper\PhoneLink'            => 'NetgluePrismicDefaults\View\Helper\Factory\PhoneLinkFactory',
                'NetgluePrismicDefaults\View\Helper\PrismicEndpoint'      => 'NetgluePrismicDefaults\View\Helper\Factory\PrismicEndpointFactory',
            ],
            'invokables' => [
                'NetgluePrismicDefaults\View\Helper\InjectFormAttributes' => 'NetgluePrismicDefaults\View\Helper\InjectFormAttributes',
                'NetgluePrismicDefaults\View\Helper\FormHelpBlock'        => 'NetgluePrismicDefaults\View\Helper\FormHelpBlock',
            ],
            'aliases' => [
                'contentSlices'        => 'NetgluePrismicDefaults\View\Helper\ContentSlices',
                'siteConfig'           => 'NetgluePrismicDefaults\View\Helper\SiteConfigHelper',
                'linkList'             => 'NetgluePrismicDefaults\View\Helper\LinkList',
                'linkResolver'         => 'NetgluePrismicDefaults\View\Helper\LinkResolverHelper',
                'bookmarkNavigation'   => 'NetgluePrismicDefaults\View\Helper\BookmarkNavigation',
                'phoneLink'            => 'NetgluePrismicDefaults\View\Helper\PhoneLink',
                'prismicEndpoint'      => 'NetgluePrismicDefaults\View\Helper\PrismicEndpoint',
                'injectFormAttributes' => 'NetgluePrismicDefaults\View\Helper\InjectFormAttributes',
                'formHelpBlock'        => 'NetgluePrismicDefaults\View\Helper\FormHelpBlock',
            ],
        ];
    }

    /**
     * Bootstrap Listener
     *
     * @param MvcEvent $event
     * @return void
     */
    public function onBootstrap(EventInterface $event)
    {
        if ($event->getRequest() instanceof HttpRequest) {
            $this->errorDocumentBootstrapListener($event);
            $this->headMetaSetupListener($event);
        }
    }

    /**
     * Bootstrap Listener
     *
     * Attaches a listener that sets up various meta data for the site
     *
     * @param MvcEvent $event
     * @return void
     */
    private function headMetaSetupListener(MvcEvent $event)
    {
        $application    = $event->getApplication();
        $serviceLocator = $application->getServiceManager();
        $listener       = $serviceLocator->get('NetgluePrismicDefaults\Mvc\Listener\HeadMetaListener');
        $application->getEventManager()->attach($listener);
    }

    /**
     * Dispatch Error Listener for Custom Error Documents
     * @param MvcEvent $event
     * @return void
     */
    private function errorDocumentBootstrapListener(MvcEvent $event)
    {
        $application    = $event->getApplication();
        $serviceLocator = $application->getServiceManager();
        $eventManager   = $application->getEventManager();
        $sharedManager  = $eventManager->getSharedManager();

        /**
         * I don't know why we have to attach to both shared and app specific manager
         */
        $eventManager->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onError'));
        $sharedManager->attach('Zend\Stdlib\DispatchableInterface', MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'onError'));
    }

    /**
     * Callback that responds to MVC Dispath Error Event
     *
     * If possible, sets the current CMS document to the error document
     *
     * @param MvcEvent $event
     * @return void
     */
    public function onError(MvcEvent $event)
    {
        $serviceLocator = $event->getApplication()->getServiceManager();
        $config = $serviceLocator->get('Config');
        $response = $event->getResponse();

        $bookmark = isset($config['prismic']['error-500-bookmark']) ? $config['prismic']['error-500-bookmark'] : null;
        if ($response->getStatusCode() == 404) {
            $bookmark = isset($config['prismic']['error-404-bookmark']) ? $config['prismic']['error-404-bookmark'] : null;
        }

        if ($bookmark) {
            $context = $serviceLocator->get('NetgluePrismic\Context');
            $document = $context->getDocumentByBookmark($bookmark);
            if ($document) {
                $manager = $serviceLocator->get('ControllerPluginManager');
                $plugin = $manager->get('Prismic');
                $plugin->setDocument($document);
            }
        }

        $view = $event->getViewModel();
        $view->errorDocument = true;
    }
}
