<?php

namespace NetgluePrismicDefaults\Mvc\Factory;

use NetgluePrismicDefaults\Mvc\Controller\ContentController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ContentControllerFactory implements FactoryInterface
{
    /**
     * Return ContentController
     * @param  ServiceLocatorInterface $controllerPluginManager
     * @return ContentController
     */
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $serviceLocator = $controllerManager->getServiceLocator();

        $controller = new ContentController;

        return $controller;
    }
}
