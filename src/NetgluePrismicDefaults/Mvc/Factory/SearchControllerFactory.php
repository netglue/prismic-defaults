<?php

namespace NetgluePrismicDefaults\Mvc\Factory;

use NetgluePrismicDefaults\Mvc\Controller\SearchController;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SearchControllerFactory implements FactoryInterface
{
    /**
     * Return SearchController
     * @param  ServiceLocatorInterface $controllerManager
     * @return SearchController
     */
    public function createService(ServiceLocatorInterface $controllerManager)
    {
        $serviceLocator = $controllerManager->getServiceLocator();

        $controller = new SearchController;
        $controller->setSearchService(
            $serviceLocator->get('NetgluePrismicDefaults\Service\SearchService')
        );

        return $controller;
    }
}
