<?php

namespace NetgluePrismicDefaults\Mvc\Factory;

use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use NetgluePrismicDefaults\Mvc\Listener\HeadMetaListener;

class HeadMetaListenerFactory implements FactoryInterface
{

    /**
     * Return configured HeadMetaListener
     * @param  ServiceLocatorInterface $serviceLocator
     * @return HeadMetaListener
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        $viewManager   = $serviceLocator->get('HttpViewManager');
        $helperManager = $viewManager->getHelperManager();
        $siteConfig    = $serviceLocator->get('NetgluePrismicDefaults\Service\SiteConfig');
        $listener = new HeadMetaListener($helperManager, $siteConfig);

        return $listener;
    }

}
