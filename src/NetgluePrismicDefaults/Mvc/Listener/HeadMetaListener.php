<?php


namespace NetgluePrismicDefaults\Mvc\Listener;

use NetgluePrismicDefaults\Service\SiteConfig;
use Prismic\Document;
use Zend\EventManager\EventInterface;
use Zend\EventManager\EventManagerInterface;
use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\ListenerAggregateTrait;
use Zend\Mvc\MvcEvent;
use Zend\View\HelperPluginManager;

class HeadMetaListener implements ListenerAggregateInterface
{
    use ListenerAggregateTrait;

    /**
     * @var HelperPluginManager View Helper Plugin Manager
     */
    protected $helperManager;

    /**
     * @var Prismic\Document
     */
    protected $document;

    /**
     * @var SiteConfig
     */
    protected $siteConfig;

    protected $mvcEvent;

    /**
     * Construct the listener. Requires the view helper plugin manager
     * @param HelperPluginManager $helperManager
     */
    public function __construct(HelperPluginManager $helperManager, SiteConfig $config)
    {
        $this->helperManager = $helperManager;
        $this->siteConfig = $config;
    }

    /**
     * Attach to specific events with the shared manager
     * @param  EventManagerInterface $events
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {
        $shared = $events->getSharedManager();
        $this->listeners[] = $shared->attach('NetgluePrismic\Mvc\Controller\Plugin\Prismic', 'setDocument', array($this, 'onSetDocument'));
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onDispatch'));
    }

    /**
     * Return HeadMeta View Helper
     * @return \Zend\View\Helper\HeadMeta
     */
    public function headMeta()
    {
        return $this->helperManager->get('headmeta');
    }

    public function onDispatch(MvcEvent $event)
    {
        $this->mvcEvent = $event;
    }

    /**
     * Document is Set Callback
     * @param  EventInterface $event
     * @return void
     */
    public function onSetDocument(EventInterface $event)
    {
        /**
         * Twitter Card Type
         * Set default type if not currently set
         */
        $currentCard = $this->findName('twitter:card');
        $defaultCard = $this->siteConfig->get('twitter_card');
        if (!$currentCard && $defaultCard) {
            $this->headMeta()->setName('twitter:card', $defaultCard);
        }

        /**
         * Twitter Image
         */
        $currentImage = $this->findName('twitter:image');
        $defaultImage = $this->siteConfig->getImageUrl('twitter_image');
        if (!$currentImage && $defaultImage) {
            $this->headMeta()->setName('twitter:image', $defaultImage);
        }

        /**
         * Twitter Site ID
         */
        $currentUser = $this->findName('twitter:site');
        $defaultUser = $this->siteConfig->get('twitter_name');
        if (!$currentUser && $defaultUser) {
            $this->headMeta()->setName('twitter:site', $defaultUser);
        }

        /**
         * Facebook Admins
         */
        $currentAdmin = $this->findName('fb:admins');
        $defaultAdmin = $this->siteConfig->get('facebook_admins');
        if (!$currentAdmin && $defaultAdmin) {
            $this->headMeta()->setProperty('fb:admins', $defaultAdmin);
        }

        /**
         * Facebook Type
         */
        $currentType = $this->findName('og:type');
        $defaultType = $this->siteConfig->get('facebook_type');
        if (!$currentType && $defaultType) {
            $this->headMeta()->setProperty('og:type', $defaultType);
        }

        /**
         * Facebook Image
         */
        $currentImage = $this->findName('og:image');
        $defaultImage = $this->siteConfig->get('facebook_image');
        if (!$currentImage && $defaultImage) {
            $this->headMeta()->setProperty('og:image', $defaultImage);
        }

        /**
         * Geo Head Meta
         */
        $lat = $this->siteConfig->getLatitude('coords');
        $lng = $this->siteConfig->getLongitude('coords');
        if ($lat && $lng) {
            $this->headMeta()->setName('geo.position', sprintf('%s;%s', $lat, $lng));
            $this->headMeta()->setName('ICBM', sprintf('%s,%s', $lat, $lng));
        }
        if ($this->siteConfig->get('geo_region')) {
            $this->headMeta()->setName('geo.region', $this->siteConfig->get('geo_region'));
        }
        if ($this->siteConfig->get('geo_place')) {
            $this->headMeta()->setName('geo.placename', $this->siteConfig->get('geo_place'));
        }
        $controllerPlugin = $event->getTarget();
        $document = $controllerPlugin->getDocument();
        if($document && $document->getType() !== 'error') {

            $prismicUrl   = $this->helperManager->get('prismicUrl');
            $canonicalUrl = null;
            try {
                $canonicalUrl = (string) $prismicUrl($document);
            } catch (\Exception $e) {

            }

            if($canonicalUrl) {
                $serverHelper = $this->helperManager->get('serverUrl');
                $canonicalUrl = $serverHelper() . $canonicalUrl;
                $this->headMeta()->setProperty('og:url', $canonicalUrl);
                $this->headMeta()->setName('twitter:url', $canonicalUrl);
                $this->headMeta()->setItemprop('url', $canonicalUrl);
                $headLinkHelper = $this->helperManager->get('headLink');
                $headLinkHelper(['rel' => 'canonical', 'href' => $canonicalUrl]);
            }

            /**
             * Schema.org Meta Data Specific to the document
             */
            $this->setSchemaMetaForDocument($document);

        }
    }

    private function setSchemaMetaForDocument(Document $document)
    {
        $docType = $document->getType();
        $name        = $document->get($docType . '.schemaorg_name')
                     ? $document->get($docType . '.schemaorg_name')->asText()
                     : null;
        $description = $document->get($docType . '.schemaorg_description')
                     ? $document->get($docType . '.schemaorg_description')->asText()
                     : null;
        $image       = $document->get($docType . '.schemaorg_image')
                     ? $document->get($docType . '.schemaorg_image')->getMain()->getUrl()
                     : null;
        foreach(['name' => $name, 'description' => $description, 'image' => $image] as $prop => $value) {
            if(!empty($value)) {
                $this->headMeta()->setItemprop($prop, $value);
            }
        }
    }

    private function isErrorDocument(EventInterface $event)
    {
        $view = $event->getViewModel();
        if(isset($view->errorDocument)) {
            return $view->errorDocument;
        }
        return false;
    }

    private function findName($name)
    {
        $container = $this->headMeta()->getContainer()->getValue();
        foreach ($container as $data) {
            if (!isset($data->type)) {
                continue;
            }
            if ($data->{$data->type} === $name) {
                return $data->content;
            }
        }
        return null;
    }
}
