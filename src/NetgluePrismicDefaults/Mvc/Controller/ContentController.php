<?php

namespace NetgluePrismicDefaults\Mvc\Controller;

class ContentController extends AbstractPrismicController
{
    /**
     * The default "content" action
     *
     * Everything relies on the route. It is used to assign the correct view
     * script file to load (Template) and requires that a single Prismic document 
     * can be located either by type, bookmark, id, uid etc.
     * 
     * @return \Zend\View\Model\ViewModel|void
     */
    public function contentAction()
    {
        $view = $this->createViewModelFromRoute();

        if (false === $view) {
            return;
        }

        return $view;
    }

    /**
     * Purge Prismic Content Cache
     *
     * @return [redirect]
     */
    public function cachePurgeAction()
    {
        $serviceLocator = $this->getServiceLocator();
        $api = $serviceLocator->get('Prismic\Api');
        $cache = $api->getCache();
        if (is_object($cache)) {
            $cache->clear();
        }
        return $this->redirect()->toUrl('/');
    }
}
