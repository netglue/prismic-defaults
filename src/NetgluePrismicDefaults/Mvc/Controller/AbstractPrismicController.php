<?php

namespace NetgluePrismicDefaults\Mvc\Controller;

use Zend\Http\Response as HttpResponse;
use Zend\Mvc\Application;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\MvcEvent;
use Zend\View\Model\ViewModel;

abstract class AbstractPrismicController extends AbstractActionController
{
    /**
     * Create a new View Model with the correct template set as defined by the current route
     * @return ViewModel|false
     */
    protected function createViewModelFromRoute()
    {
        $template = $this->getTemplateNameFromRoute();
        if (false === $template) {
            return false;
        }

        if (false === $this->getDocument()) {
            return false;
        }

        $view = new ViewModel;
        $view->setTemplate($template);

        return $view;
    }

    /**
     * Return the Prismic Document Selected in the route, or raise a 404
     * @return \Prismic\Document|false
     */
    protected function getDocument()
    {
        $doc = $this->prismic()->getDocument();
        if (!$doc) {
            $this->raise404();
            return false;
        }

        return $doc;
    }

    /**
     * Return the template name from route params.
     * If the template has not been set, setup a 404 and return false
     * @return string|false
     */
    protected function getTemplateNameFromRoute()
    {
        $viewScript = $this->params()->fromRoute('view');
        if (empty($viewScript)) {
            $this->raise404();
            return false;
        }

        return $viewScript;
    }

    /**
     * Set the response to a 404 error and trigger MvcEvent::EVENT_DISPATCH_ERROR
     * @return void
     */
    protected function raise404()
    {
        $e = $this->getEvent();
        $e->setError(Application::ERROR_CONTROLLER_INVALID);
        $response = $e->getResponse();
        if ($response instanceof HttpResponse) {
            $response->setStatusCode(404);
        }
        $this->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $this->getEvent());
    }
}
