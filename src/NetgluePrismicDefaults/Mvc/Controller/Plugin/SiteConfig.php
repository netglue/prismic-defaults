<?php

namespace NetgluePrismicDefaults\Mvc\Controller\Plugin;

use NetgluePrismicDefaults\Service\SiteConfig as Config;
use Zend\Mvc\Controller\Plugin\AbstractPlugin;

class SiteConfig extends AbstractPlugin
{
    
    /**
     * Site Wide Config Object
     * @var Config
     */
    private $config;

    /**
     * @param  Config $config
     * @return void
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * Proxy to site-wide config
     */
    public function __call($method, $args)
    {
        return call_user_func_array(array($this->config, $method), $args);
    }
    
}