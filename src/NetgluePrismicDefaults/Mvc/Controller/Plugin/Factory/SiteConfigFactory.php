<?php

namespace NetgluePrismicDefaults\Mvc\Controller\Plugin\Factory;

use NetgluePrismicDefaults\Mvc\Controller\Plugin\SiteConfig;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class SiteConfigFactory implements FactoryInterface
{
    /**
     * Return Site Config controller plugin
     * @param  ServiceLocatorInterface $controllerPluginManager
     * @return SiteConfig
     */
    public function createService(ServiceLocatorInterface $controllerPluginManager)
    {
        $serviceLocator = $controllerPluginManager->getServiceLocator();
        $config = $serviceLocator->get('NetgluePrismicDefaults\Service\SiteConfig');
        return new SiteConfig($config);
    }
}
