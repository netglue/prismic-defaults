<?php

namespace NetgluePrismicDefaults\Mvc\Controller;

use NetgluePrismicDefaults\Service\SearchService;
use Zend\Http\Response as HttpResponse;
use Zend\View\Model\ViewModel;
use Zend\Paginator\Paginator;

class SearchController extends AbstractPrismicController
{
    /**
     * @var SearchService
     */
    private $searchService;

    /**
     * Number of search results per page
     * @var int|null
     */
    private $perPage;

    /**
     * Search Query Performed
     * @var string|null
     */
    private $query;

    /**
     * Search Action
     * @return ViewModel|void
     */
    public function searchAction()
    {
        $view = $this->createViewModelFromRoute();
        $doc  = $this->prismic()->getDocument();

        /**
         * We need a dedicated document from route params
         * So that the view can setup the search page
         */
        if (!$view || !$doc) {
            return;
        }



        $page    = $this->params()->fromRoute('page');
        $query   = $this->params()->fromQuery('q');
        if (empty($query)) {
            $query = $this->params()->fromRoute('q');
        }

        $view->pager = null;

        /**
         * Make the search query available in layout and view
         */
        $view->searchQuery = $query;
        $this->layout()->searchQuery = $query;
        $this->query = $query;

        if (!empty($query)) {
            $view->pager = $this->searchService->search($query, $this->getResultCountPerPage());
            $view->pager->setCurrentPageNumber($page);

            $this->setHeadLinks($view->pager);
        }

        return $view;
    }

    /**
     * Helper method sets rel next and prev links in the head
     *
     * Absolute URLs are determined as this appears to be best practice SEO wise
     *
     * @param Paginator $pager
     * @return void
     */
    private function setHeadLinks(Paginator $pager)
    {
        $pluginManager = $this->getServiceLocator()->get('viewhelpermanager');
        $headLink = $pluginManager->get('headLink');
        $url = $pluginManager->get('url');
        $server = $pluginManager->get('serverUrl');
        $serverUrl = (string) $server();

        $currentPage = $pager->getCurrentPageNumber();

        if($currentPage > 1) {
            $prev = $pager->getCurrentPageNumber() - 1;
            $prevUrl = $serverUrl . $url('netglue-search/paged', ['page' => $prev, 'q' => $this->query]);
            $headLink(['rel' => 'prev', 'href' => $prevUrl]);
        }

        if($currentPage < count($pager)) {
            $next = $pager->getCurrentPageNumber() + 1;
            $nextUrl = $serverUrl . $url('netglue-search/paged', ['page' => $next, 'q' => $this->query]);
            $headLink(['rel' => 'next', 'href' => $nextUrl]);
        }
    }

    /**
     * Return the number of results expected per page
     * @return int
     */
    public function getResultCountPerPage()
    {
        if (is_null($this->perPage)) {
            $this->perPage = 10;
            $configured = $this->siteConfig()->get('search_count');
            if (is_numeric($configured) && $configured > 0) {
                $this->perPage = (int) $configured;
            }
        }
        return $this->perPage;
    }

    /**
     * Set Search Service
     * @param SearchService $service
     * @return void
     */
    public function setSearchService(SearchService $service)
    {
        $this->searchService = $service;
    }

    /**
     * Set Num results per page
     * @param int $perPage
     * @return void
     */
    public function setResultCountPerPage($perPage)
    {
        $this->perPage = (int) $perPage;
    }
}
