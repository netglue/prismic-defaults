<?php

namespace NetgluePrismicDefaults\Model;

use NetgluePrismicDefaults\Common\LinkResolverAwareTrait;
use Prismic\Document;

class FormConfig
{
    use LinkResolverAwareTrait;

    private $config;

    protected static $fieldsFragment = 'fields';

    public function __construct(Document $config)
    {
        $this->config = $config;
    }

    /**
     * @return array
     */
    private function getFields()
    {
        $type = $this->config->getType();
        $fragName = sprintf('%s.%s', $type, static::$fieldsFragment);
        if($frag = $this->config->get($fragName)) {
            return $frag->getArray();
        }
        return [];
    }

    /**
     * @return array|null
     */
    public function getFieldConfig($name)
    {
        foreach($this->getFields() as $field) {
            if(isset($field['name']) && $field['name']->asText() === $name) {
                return $field;
            }
        }
    }

    private function getPropertyText($prop, $name)
    {
        if($data = $this->getFieldConfig($name)) {
            if(isset($data[$prop])) {
                return $data[$prop]->asText();
            }
        }
    }

    private function getPropertyHtml($prop, $name)
    {
        if($data = $this->getFieldConfig($name)) {
            if(isset($data[$prop])) {
                return $data[$prop]->asHtml($this->getLinkResolver());
            }
        }
    }

    public function getLabel($name)
    {
        return $this->getPropertyText('label', $name);
    }

    public function getPlaceholder($name)
    {
        return $this->getPropertyText('placeholder', $name);
    }

    public function getHelpBlock($name)
    {
        return $this->getPropertyHtml('help', $name);
    }

}
