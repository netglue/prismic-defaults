{
    "Details" : {
        "config_name" : {
            "type" : "StructuredText",
            "fieldset" : "Name this configuration set in case you ever setup more than one",
            "config" : {
                "single" : "heading1",
                "placeholder" : "Site-Wide Configuration"
            }
        },
        "copyright_year" : {
            "type" : "Number",
            "fieldset" : "The year in which this content was first published",
            "config" : {
                "label" : "Copyright Year",
                "min": 1950,
                "max": 2200
            }
        }
    },
    "Company Info" : {
        "name" : {
            "type" : "Text",
            "fieldset" : "The company name as you want it to appear in most situations",
            "config" : {
                "label" : "Company Name",
                "placeholder" : ""
            }
        },
        "legal_name" : {
            "type" : "Text",
            "fieldset" : "The legal company name such as 'Widgets Ltd t/as Foo Bar'",
            "config" : {
                "label" : "Legal Name",
                "placeholder" : ""
            }
        },
        "company_number" : {
            "type" : "Text",
            "fieldset" : "Company registration number",
            "config" : {
                "label" : "Company Number",
                "placeholder" : "123456789"
            }
        },
        "vat_number" : {
            "type" : "Text",
            "fieldset" : "VAT registration number",
            "config" : {
                "label" : "VAT Number",
                "placeholder" : "123456789"
            }
        },
        "phone" : {
            "type" : "Text",
            "fieldset" : "Your Phone number",
            "config" : {
                "label" : "Phone Number",
                "placeholder" : "+44 01234 567 890"
            }
        },
        "email" : {
			"type" : "Text",
			"fieldset" : "Your General Email Address",
			"config" : {
				"label" : "Email Address",
				"placeholder" : "you@example.com"
			}
		},
		"website" : {
			"type" : "Text",
			"fieldset" : "Your website domain name as you want it to appear",
			"config" : {
				"label" : "Website",
				"placeholder" : "www.example.com"
			}
		}
	},
	"Structured Data" : {
		"schema_business_type" : {
		    "type" : "Text",
		    "fieldset" : "Most appropriate schema.org business type",
		    "config" : {
		        "label" : "Business Type",
		        "placeholder" : "i.e. FoodEstablishment, ProfessionalService, Retaurant etc"
		    }
		},
		"schema_business_description" : {
		    "type" : "Text",
		    "fieldset" : "General business description included in the footer of every page",
		    "config" : {
		        "label" : "Business Description"
		    }
		},
		"schema_business_logo" : {
		    "type" : "Image",
		    "fieldset" : "A Hidden image that represents your logo"
		}
    },
    "Location" : {
		"address" : {
			"type" : "Text",
			"fieldset" : "Postal Address, formatted as you want it to be",
			"config" : {
				"label" : "Address",
				"placeholder" : ""
			}
		},
		"address_street":{
			"type": "Text",
			"fieldset": "Street Address",
			"config": {
				"label": "Street Address",
				"placeholder": ""
			}
		},
		"address_town":{
			"type": "Text",
			"fieldset": "Town/City",
			"config": {
				"label": "Town/City",
				"placeholder": ""
			}
		},
		"address_region":{
			"type": "Text",
			"fieldset": "Region/County",
			"config": {
				"label": "Region/County",
				"placeholder": ""
			}
		},
		"address_postcode":{
			"type": "Text",
			"fieldset": "Postcode",
			"config": {
				"label": "Postcode",
				"placeholder": ""
			}
		},
		"address_country":{
			"type": "Text",
			"fieldset": "Country",
			"config": {
				"label": "Country",
				"placeholder": "United Kingdom"
			}
		},
        "coords" : {
            "type" : "GeoPoint",
            "fieldset" : "Map Coordinates for your business - added to the meta of the site to help SEO"
        },
        "geo_region" : {
            "type" : "Text",
            "fieldset" : "Geo Coded Region: Meta data to help search engines localise you",
            "config" : {
                "label" : "Geo Coded Region"
            }
        },
        "geo_place" : {
            "type" : "Text",
            "fieldset" : "Geo Coded Place Name: Meta data to help search engines localise you",
            "config" : {
                "label" : "Geo Coded Place Name"
            }
        },
        "google_maps_embed_key" : {
            "type" : "Text",
            "fieldset" : "Google Maps Embed API Key",
            "config" : {
                "label" : "Google Maps Embed API Key"
            }
        }
    },
    "Social" : {
        "twitter_name" : {
            "type" : "Text",
            "fieldset" : "Your twitter name without the @ symbol"
        },
        "twitter_card" : {
			"type" : "Select",
			"fieldset": "Default Twitter Card Type",
			"config" : {
				"label" : "Default Twitter Card Type",
				"options" : ["summary", "summary_large_image", "photo"]
			}
		},
		"twitter_image" : {
		    "type" : "Image",
		    "fieldset" : "Default Twitter Card Image",
		    "config" : {
		        "constraint" : {
		            "width" : 1200
		        }
		    }
		},
		"facebook_url" : {
            "type" : "Link",
            "fieldset" : "URL for your Facebook profile",
            "config" : {
                "select" : "web",
                "placeholder" : "Leave blank to remove Facebook links"
            }
        },
        "facebook_admins" : {
            "type" : "Text",
            "fieldset" : "Add comma separated facebook admin ids here",
            "config" : {
                "label" : "Facebook Admins",
                "placeholder" : "23245839,749437282"
            }
        },
        "facebook_app_id" : {
            "type" : "Text",
            "fieldset" : "Facebook App ID",
            "config" : {
                "label" : "Facebook App ID",
                "placeholder" : "23245839"
            }
        },
        "facebook_type" : {
            "type" : "Text",
            "fieldset" : "Default Facebook object type",
            "config" : {
                "label" : "Default Type",
                "placeholder" : "website"
            }
        },
        "facebook_image" : {
            "type" : "Image",
		    "fieldset" : "Default Facebook Share Image",
		    "config" : {
		        "constraint" : {
		            "width" : 1200,
		            "height" : 630
		        }
		    }
        },
        "google_plus_url" : {
            "type" : "Link",
            "fieldset" : "URL for your Google+ profile",
            "config" : {
                "select" : "web",
                "placeholder" : "Leave blank to remove Google+ links"
            }
        },
        "pinterest_url" : {
            "type" : "Link",
            "fieldset" : "URL for your Pinterest profile",
            "config" : {
                "select" : "web",
                "placeholder" : "Leave blank to remove Pinterest links"
            }
        },
        "instagram_url" : {
            "type" : "Link",
            "fieldset" : "URL for your Instagram profile",
            "config" : {
                "select" : "web",
                "placeholder" : "Leave blank to remove Instagram links"
            }
        },
        "linkedin_url" : {
            "type" : "Link",
            "fieldset" : "URL for your LinkedIn profile",
            "config" : {
                "select" : "web",
                "placeholder" : "Leave blank to remove LinkedIn links"
            }
        }
    },
    "Search" : {
        "search_count" : {
            "type" : "Number",
            "fieldset" : "Search Results Per Page",
            "config" : {
                "label" : "Results Per Page",
                "min" : 1,
                "max" : 50
            }
        }
    },
    "Layout" : {
        "head_logo" : {
            "type" : "Image",
            "fieldset" : "The company logo used in the site header"
        }
    }
}
