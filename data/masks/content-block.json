{
    "Details" : {
        "name" : {
            "type" : "StructuredText",
            "fieldset": "Name this block of re-useable content",
            "config" : {
                "single" : "heading1,em,strong",
                "placeholder" : "This will help you find it again!"
            }
        },
        "description" : {
            "type" : "StructuredText",
            "fieldset" : "Provide a description or some notes",
            "config" : {
                "multi" : "paragraph,em,strong"
            }
        }
    },
    "Content" : {
        "body" : {
            "fieldset" : "Build up the page content by adding blocks",
            "type" : "Slices",
            "config" : {
                "labels" : {
                    "featured-items" : [ {
                        "name" : "stacked",
                        "display" : "Featured Items Stacked Vertically"
                    } ],
                    "block-heading" : [
                        {
                            "name" : "tall",
                            "display" : "Tall"
                        },
                        {
                            "name" : "short",
                            "display" : "Short"
                        }
                    ],
                    "carousel" : [
                        {
                            "display" : "Include Page Dots",
                            "name" : "dots"
                        },
                        {
                            "display" : "Include Prev/Next Buttons",
                            "name" : "buttons"
                        },
                        {
                            "display" : "Include Dots & Buttons",
                            "name" : "dots-buttons"
                        }
                    ],
                    "call-to-action" : [
                        {
                            "display" : "Wide Content",
                            "name" : "wide"
                        },
                        {
                            "display" : "Narrow Content",
                            "name" : "narrow"
                        }
                    ],
                    "featured-links" : [
                        { "display" : "Yellow", "name" : "Yellow" },
                        { "display" : "Blue", "name" : "Blue" },
                        { "display" : "Green", "name" : "Green" },
                        { "display" : "Orange", "name" : "Orange" },
                        { "display" : "Red", "name" : "Red" },
                        { "display" : "Grey", "name" : "Grey" },
                        { "display" : "Black", "name" : "Black" },
                        { "display" : "White", "name" : "White" }
                    ]
                },
                "choices" : {
                    "carousel" : {
                        "type" : "Group",
                        "fieldset" : "Carousel",
                        "config" : {
                            "fields" : {
                                "image" : {
                                    "type" : "Image",
                                    "fieldset" : "Choose an image that works in a wide, landscape context. Aspect Ratio 4:1",
                                    "config" : {
                                        "placeholder" : "Choose the slide background image.",
                                        "constraint" : {
                                            "width" : 1500,
                                            "height" : 375
                                        }
                                    }
                                },
                                "text_color" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Text Color",
                                        "options" : ["Dark", "Light"]
                                    }
                                },
                                "title" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "Add a title to the slide to overlay the image",
                                    "config" : {
                                        "placeholder" : "Slide Title: Add a title to the slide to overlay the image",
                                        "single":"heading2,heading1,em,strong"
                                    }
                                },
                                "leader" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "Page introduction: Large type, relatively short. Appears beneath the heading",
                                    "config" : {
                                        "placeholder" : "Slide Subtitle: Large type, relatively short. Appears beneath the heading",
                                        "single" : "paragraph,em,strong"
                                    }
                                },
                                "link_text" : {
                                    "type" : "Text",
                                    "config" : {
                                        "label" : "Link Text",
                                        "placeholder" : "i.e. 'Click Here…'"
                                    }
                                },
                                "button_theme" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Button Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "link" : {
                                    "type" : "Link",
                                    "config" : {
                                        "label" : "Link URL",
                                        "placeholder" : "Choose where this link goes"
                                    }
                                }
                            }
                        }
                    },
                    "banner" : {
                        "type" : "Group",
                        "fieldset" : "Banner Image",
                        "config" : {
                            "fields" : {
                                "image" : {
                                    "type" : "Image",
                                    "fieldset" : "Banner Image. Choose an image that works in a wide, landscape context",
                                    "config" : {
                                        "placeholder" : "Banner Image. Choose an image that works in a wide, landscape context",
                                        "constraint" : {
                                            "width" : 1500
                                        }
                                    }
                                },
                                "title" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "The main page heading",
                                    "config" : {
                                        "single" : "heading2,heading1,em,strong,hyperlink",
                                        "placeholder" : "A good page heading. Important for SEO"
                                    }
                                },
                                "leader" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "Page introduction: Large type, relatively short. Appears beneath the heading",
                                    "config" : {
                                        "placeholder" : "Page introduction: Large type, relatively short. Appears beneath the heading",
                                        "single" : "paragraph,em,strong,hyperlink"
                                    }
                                },
                                "caption_position" : {
                                    "type" : "Select",
                                    "fieldset" : "Caption Position",
                                    "config" : {
                                        "label": "Caption Position",
                                        "options" : ["Left", "Bottom-Center", "Top-Center", "Center", "Right"]
                                    }
                                },
                                "caption_theme" : {
                                    "type" : "Select",
                                    "fieldset" : "Caption Theme",
                                    "config" : {
                                        "label" : "Caption Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "button_text" : {
                                    "type" : "Text",
                                    "config" : {
                                        "label" : "Button Text",
                                        "placeholder" : "Optional Button Text"
                                    }
                                },
                                "button_theme" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Button Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "button_link" : {
                                    "type" : "Link",
                                    "config" : {
                                        "label" : "Button URL",
                                        "placeholder" : "Optional button link"
                                    }
                                }
                            }
                        }
                    },
                    "featured-items" : {
                        "type" : "Group",
                        "fieldset" : "Featured Items",
                        "config" : {
                            "fields" : {
                                "theme" : {
                                    "fieldset" : "Select a theme for the featured item",
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "background" : {
                                    "fieldset" : "Optionally, select a background image",
                                    "type" : "Image",
                                    "config" : {
                                        "placeholder" : "Optionally, select a background image"
                                    }
                                },
                                "background_type" : {
                                    "fieldset" : "Is your background image a pattern?",
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Background Type",
                                        "options" : ["Pattern", "Photo"]
                                    }
                                },
                                "image" : {
                                    "type" : "Image",
                                    "fieldset" : "Choose an image for the item or link",
                                    "config" : {
                                        "placeholder" : "Choose an image for the item or link",
                                        "constraint" : {
                                            "width" : 800
                                        }
                                    }
                                },
                                "title" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "A title for the featured link",
                                    "config" : {
                                        "single" : "heading2,em,strong,hyperlink",
                                        "placeholder" : "A title for the featured link"
                                    }
                                },
                                "description" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "One or two paragraphs of introductory text",
                                    "config" : {
                                        "multi": "paragraph,em,strong,hyperlink",
                                        "placeholder" : "Introductory text to this featured link, beneath the title"
                                    }
                                },
                                "button" : {
                                    "type" : "Text",
                                    "config" : {
                                        "label" : "Button Text",
                                        "placeholder" : "Button Link Text, i.e. 'Learn More…'"
                                    }
                                },
                                "button_theme" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Button Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "link" : {
                                    "type" : "Link",
                                    "config" : {
                                        "label" : "Destination",
                                        "placeholder" : "Button Link: Select a destination"
                                    }
                                }
                            }
                        }
                    },
                    "separator" : {
                        "type" : "Group",
                        "fieldset" : "Separator",
                        "config" : {
                            "fields" : {
                                "theme" : {
                                    "type" : "Select",
                                    "fieldset" : "Choose the separator theme",
                                    "config" : {
                                        "label" : "Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "size" : {
                                    "type" : "Select",
                                    "fieldset" : "Size/Height of the separator",
                                    "config" : {
                                        "label" : "Size",
                                        "options" : [ "Small", "Medium", "Large" ]
                                    }
                                },
                                "image" : {
                                    "type" : "Image",
                                    "fieldset" : "Optionally, select a background image",
                                    "config" : {
                                        "constraint" : {
                                        }
                                    }
                                },
                                "image_type" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Image Type",
                                        "options" : ["Pattern", "Photo"]
                                    }
                                }
                            }
                        }
                    },
                    "content" : {
                        "type" : "StructuredText",
                        "fieldset" : "Content / Body Copy Section",
                        "config" : {
                            "imageConstraint" : {
                                "width" : "1200"
                            },
                            "labels" : ["large-paragraph", "quote"]
                        }
                    },
                    "block-heading" : {
                        "type" : "Group",
                        "fieldset" : "Block Heading",
                        "config" : {
                            "fields" : {
                                "theme" : {
                                    "type" : "Select",
                                    "fieldset" : "Choose the heading theme",
                                    "config" : {
                                        "label" : "Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "heading" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "Heading Text",
                                    "config" : {
                                        "single" : "heading1,heading2,heading3,heading4,heading5,em,strong,hyperlink",
                                        "placeholder" : "Heading: Only use Level 1 when there isn’t one anywhere else…"
                                    }
                                },
                                "subtitle" : {
                                    "type" : "StructuredText",
                                    "fieldset": "Optional leader text / subtitle",
                                    "config" : {
                                        "single" : "paragraph,em,strong,hyperlink",
                                        "placeholder" : "Subtitle: Intro paragraph in a large font. Completely Optional…"
                                    }
                                }
                            }
                        }
                    },
                    "faq-section" : {
                        "type" : "Group",
                        "fieldset" : "FAQ List",
                        "config" : {
                            "fields" : {
                                "question" : {
                                    "type" : "Text",
                                    "fieldset" : "Question Text",
                                    "config" : {
                                        "label" : "Question",
                                        "placeholder" : "(Required)"
                                    }
                                },
                                "answer" : {
                                    "type" : "StructuredText",
                                    "fieldset": "Answer the question…",
                                    "config" : {
                                        "label" : "Answer",
                                        "multi" : "paragraph,em,strong,hyperlink,olist,ulist,list-item,o-list-item",
                                        "placeholder" : "(Required)"
                                    }
                                }
                            }
                        }
                    },
                    "call-to-action" : {
                        "type" : "Group",
                        "fieldset" : "Call to action",
                        "config" : {
                            "fields" : {
                                "theme" : {
                                    "type" : "Select",
                                    "fieldset" : "Choose the call to action theme",
                                    "config" : {
                                        "label" : "Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "title" : {
                                    "type" : "StructuredText",
                                    "config" : {
                                        "placeholder" : "Title Text",
                                        "single" : "heading2, em, strong"
                                    }
                                },
                                "description" : {
                                    "type" : "StructuredText",
                                    "config" : {
                                        "placeholder" : "Intro Paragraph",
                                        "multi" : "paragraph, em, strong, hyperlink"
                                    }
                                },
                                "button" : {
                                    "type" : "Text",
                                    "config" : {
                                        "label" : "Button Text",
                                        "placeholder" : "Button Text, i.e. 'Learn More…'"
                                    }
                                },
                                "button_theme" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Button Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "link" : {
                                    "type" : "Link",
                                    "config" : {
                                        "label" : "Button Link",
                                        "placeholder" : "Choose where the button goes"
                                    }
                                },
                                "image" : {
                                    "type" : "Image",
                                    "config" : {
                                        "constraint" : {
                                        },
                                        "placeholder" : "Optional Background Image"
                                    }
                                },
                                "image_type" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Image Type",
                                        "options" : ["Pattern", "Photo"]
                                    }
                                }
                            }
                        }
                    },
                    "contact-form" : {
                        "type" : "Group",
                        "fieldset" : "Contact Us Form",
                        "config" : {
                            "fields" : {
                                "theme" : {
                                    "type" : "Select",
                                    "fieldset" : "Choose the form theme",
                                    "config" : {
                                        "label" : "Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "form-config" : {
                                    "type" : "Link",
                                    "config" : {
                                        "placeholder" : "Select form configuration",
                                        "select" : "document",
                                        "masks" : [ "form-config" ]
                                    }
                                }
                            }
                        }
                    },
                    "search-form" : {
                        "fieldset" : "Website Search Form",
                        "type" : "Select",
                        "config" : {
                            "label" : "Theme",
                            "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                        }
                    },
                    "subscribe-form" : {
                        "fieldset" : "Mailing List Subscribe Form",
                        "type" : "Select",
                        "config" : {
                            "label" : "Theme",
                            "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                        }
                    },
                    "code-block" : {
                        "fieldset" : "Code Block",
                        "type" : "Group",
                        "config" : {
                            "fields" : {
                                "language" : {
                                    "type" : "Select",
                                    "fieldset" : "Choose the language of the code or text",
                                    "config" : {
                                        "label" : "Language",
                                        "options" : ["Text", "HTML", "CSS", "JavaScript", "JSON", "PHP", "Ruby", "Python", "C", "SQL"]
                                    }
                                },
                                "code" : {
                                    "type" : "StructuredText",
                                    "fieldset" : "Paste in your code",
                                    "config" : {
                                        "placeholder" : "Paste in your code here…",
                                        "single" : "preformatted"
                                    }
                                }
                            }
                        }
                    },
                    "featured-links" : {
                        "fieldset" : "Featured Links (Horizontal)",
                        "type" : "Group",
                        "config" : {
                            "fields" : {
                                "title" : {
                                    "type" : "StructuredText",
                                    "config" : {
                                        "placeholder" : "Link Title",
                                        "single" : "heading2, em, strong"
                                    }
                                },
                                "description" : {
                                    "type" : "StructuredText",
                                    "config" : {
                                        "placeholder" : "Link Description",
                                        "single" : "paragraph, em, strong, hyperlink"
                                    }
                                },
                                "button_text" : {
                                    "type" : "Text",
                                    "config" : {
                                        "label" : "Button Text",
                                        "placeholder" : "i.e. 'Click Here!'"
                                    }
                                },
                                "button_theme" : {
                                    "type" : "Select",
                                    "config" : {
                                        "label" : "Button Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                },
                                "link" : {
                                    "type" : "Link",
                                    "config" : {
                                        "label" : "Button Link",
                                        "placeholder" : "Select where the link will go"
                                    }
                                },
                                "image" : {
                                    "type" : "Image",
                                    "config" : {
                                        "constraint" : {
                                            "width" : 400,
                                            "height" : 400
                                        },
                                        "placeholder" : "Icon or Image"
                                    }
                                }
                            }
                        }
                    },
                    "quote" : {
                        "fieldset" : "Quotes/Testimonials",
                        "type" : "Group",
                        "config" : {
                            "fields" : {
                                "quote" : {
                                    "type" : "Link",
                                    "config" : {
                                        "select" : "document",
                                        "masks" : ["quote"],
                                        "placeholder" : "Select the quote or testimonial"
                                    }
                                },
                                "theme" : {
                                    "type" : "Select",
                                    "fieldset" : "Choose the heading theme",
                                    "config" : {
                                        "label" : "Theme",
                                        "options" : [ "Yellow", "Blue", "Green", "Orange", "Red", "Grey", "Black", "White" ]
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
